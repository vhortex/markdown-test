## Things to know about this repository

Things to do

* make sure to change to the correct branch
* run **yarn** on fist run in order to get the correct modules installed. Used modules are also copied to ** Z:\guest\Node-Modules-For-Apps\ **

**git branch -a** - list all branches

**git checkout android-build** - change to *android-build*

**react-native -v** - check react-native-cli version and react-native version

---

## React Native Information

1. **react-native-cli:** 2.0.1
2. **react-native:** 0.59.10
3. **branch:** android-build

---

## Run any project 

Running on IOS

1. Open up project directory
2. Run **react-native link**, this will connect all modules and projects that we need as symlinks
3. Change directory to **ios**
4. Run **pod install**, this will install all needed modules
5. Open Xcode and open the project. Click *Run*

Running on Android

1. Open up project directory
2. Run **react-native link**, this will connect all modules and projects that we need as symlinks
3. Open Android Studio and perform *gradle sync*
4. Run your project

**Note:** If modules are missing, run **yarn**, please note that you need to install *yarn* on your development machine

---
## Create a Release Build

*For iOS* 

1. Setup certificates 
2. Select generic ios device from the device lis 
3. Go to product from top menu and click archive 
4. Once archive is done then an organizer window will popup, click on distribute and follow instructions to complete submit build on testflight 
5. Go to appstoreconnect.apple.com
6. Select project and create new release for iOS 
7. Then select uploaded build in build section 
8. Click save and submit

*For Android*

1. Select build from top menu and click ‘generate signed apk / bundle’ 
2. Enter keystore filepath, alias and password 
3. Select release build 
4. Select both signing methods 
5. Click submit 
6. After build is generated, click locate build 
7. Copy build and login to playstore 
8. Create new release build on playsore and upload the generated build 
9. Click save and review and then submit

---
## Common Issues

If this is the first clone, make sure to run “yarn” on the project folder, this will fix the dependencies

**Frequent issues**

*  Android – apkdata not found – Please clean and rebuild in android studio
*  iOS – JS malformed – Please clean build and delete librabry/xcode/derived data folder
*  iOS Linking error – Please run react-native link command
*  Android – package not found - Please run react-native link command

---

## Metro Bundler Usage

To run any project with simulator either on iOS or Android, ensure you have started metro bundler for that particular project. 

*Steps to do so*

1. Open terminal / command prompt  
2. Navigate to project folder  
3. Run command - yarn start 
4. Then wait till you see listing on port 8081 
